.equ zp_first_line, 0			; 2 bytes
.equ zp_current_line, 2			; 2 bytes
.equ zp_tmp, 4					; 2 bytes
.equ zp_byte_tmp, 6				; 1 byte
.equ zp_actual_line_start, 7	; 1 byte
.equ zp_line_ptr, 8				; 2 bytes
.equ zp_prev_line, 10			; 2 bytes
.equ zp_next_line, 12			; 2 bytes
.equ zp_next_alloc, 14			; 2 bytes

.equ zp_word_a, 16				; 2 bytes, also stores result
.equ zp_word_b, 18				; 2 bytes
.equ zp_arith_tmp, 20			; 2 bytes

.equ zp_stmt_str, 32			; 2 bytes
.equ zp_line2, 34				; 2 bytes
.equ zp_error_code, 36			; 1 byte

.equ OK, 0
.equ ERR_UNK, 1

; error codes:
; 	0 - no errors
;	1 - unknown command
