; flag C = 1 if number = 0
word_is_0:
	ld %zp_word_a
	cmp #0
	jne .end
	ld %zp_word_a + 1
	cmp #0
	jne .end
	stc
.end:
	ret

; flag C = 1 if a < b
word_lt:
	ld %zp_word_a + 1
	cmp %zp_word_b + 1
	jcs .yes
	jne .no
	ld %zp_word_a
	cmp %zp_word_b
	jcs .yes
.no:
	clc
	ret
.yes:
	stc
	ret

word_mul:
	ld #0
	st %zp_arith_tmp
	st %zp_arith_tmp + 1

.loop:
	call word_is_0
	jcs .end

	ld %zp_arith_tmp
	adc %zp_word_b
	st %zp_arith_tmp

	ld %zp_arith_tmp + 1
	adc %zp_word_b + 1
	st %zp_arith_tmp + 1

	clc
	ld %zp_word_a
	sbc #1
	st %zp_word_a
	ld %zp_word_a + 1
	sbc #0
	st %zp_word_a + 1

	jmp .loop
.end:
	ld %zp_arith_tmp
	st %zp_word_a
	ld %zp_arith_tmp + 1
	st %zp_word_a + 1
	ret

word_div:
	ld #0
	st %zp_arith_tmp
	st %zp_arith_tmp + 1

.loop:
	call word_lt
	jcs .end

	; increment result
	clc
	ld %zp_arith_tmp
	adc #1
	st %zp_arith_tmp
	ld %zp_arith_tmp + 1
	adc #0
	st %zp_arith_tmp + 1

	; subtract a - b
	clc
	ld %zp_word_a
	sbc %zp_word_b
	st %zp_word_a
	ld %zp_word_a + 1
	sbc %zp_word_b + 1
	st %zp_word_a + 1

	jmp .loop

.end:
	; remainder
	ld %zp_word_a
	st %zp_word_b
	ld %zp_word_a + 1
	st %zp_word_b + 1

	; quotient
	ld %zp_arith_tmp
	st %zp_word_a
	ld %zp_arith_tmp + 1
	st %zp_word_a + 1

	ret
