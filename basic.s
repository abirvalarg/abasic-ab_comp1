eval_line:
	ldy
	push
	ld #0
	sty

.skip_spaces_loop:
	ld [zp_stmt_str], y
	cmp #' '
	jne .skip_spaces_loop_end
	cmp #0
	jeq .skip_spaces_loop_end
	iny
	jmp .skip_spaces_loop
.skip_spaces_loop_end:

	; empty?
	ld #0
	st %zp_error_code
	cmp [zp_stmt_str], y
	jeq .end

	; `LIST`?
	ld #kw_list
	st %zp_line2
	ld #kw_list >> 8
	st %zp_line2 + 1
	call str_starts_with
	jcs .list

	; `NEW`?
	ld #kw_new
	st %zp_line2
	ld #kw_new >> 8
	st %zp_line2 + 1
	call str_starts_with
	jcs .new

	jmp .unknown_command

.list:
	ld %zp_first_line
	st %zp_current_line
	ld %zp_first_line + 1
	st %zp_current_line + 1

.list_loop:
	ld %zp_current_line + 1
	cmp #0
	jeq .end

	ld #1
	sty
	ld [zp_current_line]
	st %zp_next_line
	ld [zp_current_line], y
	st %zp_next_line + 1
	iny
	ld [zp_current_line], y
	st %zp_word_a
	iny
	ld [zp_current_line], y
	st %zp_word_a + 1
	call send_number_a

	clc
	ld %zp_current_line
	adc #4
	st %zp_line_ptr
	ld %zp_current_line + 1
	adc #0
	st %zp_line_ptr + 1
	call send_line

	ld #'\r'
	call send_byte
	ld #'\n'
	call send_byte

	ld %zp_next_line
	st %zp_current_line
	ld %zp_next_line + 1
	st %zp_current_line + 1
	jmp .list_loop

.new:
	ld #0
	st %zp_first_line
	st %zp_first_line + 1
	ld #lines
	st %zp_next_alloc
	ld #lines >> 8
	st %zp_next_alloc + 1
	jmp .end

.unknown_command:
	ld #1
	st %zp_error_code

.end:
	pop
	sty
	ret

; target str: `[zp_stmt_str], y`
; other str: `[zp_line2]`
; destroys the address in `zp_line2`
str_starts_with:
	ldy
	push

.loop:
	ld [zp_line2]
	cmp #0
	jeq .yes
	cmp [zp_stmt_str], y
	jne .no
	iny
	clc
	ld %zp_line2
	adc #1
	st %zp_line2
	ld %zp_line2 + 1
	adc #0
	st %zp_line2 + 1
	jmp .loop

.no:
	clc
	jmp .end

.yes:
	stc

.end:
	pop
	sty
	ret

errors:
	.word 0
	.word err_msg_unknown

kw_print:
	.str "PRINT\0"

kw_goto:
	.str "GOTO\0"

kw_list:
	.str "LIST\0"

kw_new:
	.str "NEW\0"

err_msg_unknown:
	.str "UNK\r\n\0"
