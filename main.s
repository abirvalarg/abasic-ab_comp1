.include "zp.s"

.org 0x0200
start:
	; clear first line
	ld #0
	st %zp_first_line
	st %zp_first_line + 1
	ld #lines
	st %zp_next_alloc
	ld #lines >> 8
	st %zp_next_alloc + 1

.run_loop:
	ld #0
	st %zp_word_b
	st %zp_word_b + 1
	sty
.loop:
	call read_byte
	cmp #'\r'
	jeq .process_line
	cmp #0x7f
	jeq .bsp

	stx
	call is_letter
	ldx
	jcc .not_letter_input
	and #~' '	; upcase

.not_letter_input:
	call send_byte
	st input_buffer, y
	iny
	ldy
	cmp #81
	jeq .overflow
	jmp .loop

.process_line:
	call send_byte
	ld #'\n'
	call send_byte

	ld #0
	st input_buffer, y

	ld input_buffer
	call is_digit
	jcs .append_program

	ld #input_buffer
	st %zp_stmt_str
	ld #input_buffer >> 8
	st %zp_stmt_str + 1
	call eval_line

	ld %zp_error_code
	cmp #0
	jeq .run_loop

	; error accured
	lsl
	stx
	ld errors, x
	st %zp_line_ptr
	ld errors + 1, x
	st %zp_line_ptr + 1
	call send_line

	jmp .run_loop

.append_program:
	ld #0
	sty
	iny
	st %zp_word_b + 1
	ld input_buffer
	clc
	sbc #'0'
	st %zp_word_b

.append_program_number_loop:
	ld input_buffer, y
	call is_digit
	jcc .append_program_number_loop_end
	stx
	ld #10
	st %zp_word_a
	ld #0
	st %zp_word_a + 1
	call word_mul
	ldx
	clc
	sbc #'0'
	adc %zp_word_a
	st %zp_word_b
	ld %zp_word_a + 1
	adc #0
	st %zp_word_b + 1
	iny
	jmp .append_program_number_loop
.append_program_number_loop_end:

	ldy
	st %zp_actual_line_start

	; find the line to insert to
	ld #0
	st %zp_prev_line
	st %zp_prev_line + 1
	ld %zp_first_line
	st %zp_current_line
	ld %zp_first_line + 1
	st %zp_current_line + 1

.append_search_loop:
	ld #0
	cmp %zp_current_line
	jne .append_search_loop_continue
	cmp %zp_current_line + 1
	jne .append_search_loop_continue
	; found the last line

	ld #1
	sty
	; next line
	ld #0
	st [zp_next_alloc]
	st [zp_next_alloc], y
	iny
	; line number
	ld %zp_word_b
	st [zp_next_alloc], y
	iny
	ld %zp_word_b + 1
	st [zp_next_alloc], y
	iny

	; copy data
	ld %zp_actual_line_start
	stx
.copy_last_loop:
	ld input_buffer, x
	st [zp_next_alloc], y
	inx
	iny
	cmp #0
	jne .copy_last_loop

	ld %zp_prev_line + 1
	cmp #0
	jne .last_not_first

	ld %zp_next_alloc
	st %zp_first_line
	ld %zp_next_alloc + 1
	st %zp_first_line + 1
	jmp .after_last_and_first

.last_not_first:
	ld #1
	sty
	ld %zp_next_alloc
	st [zp_prev_line]
	ld %zp_next_alloc + 1
	st [zp_prev_line], y

.after_last_and_first:
	call advance_alloc

	jmp .run_loop

.append_search_loop_continue:

	; check if the current line has the number that was entered
	ld #2
	sty
	ld [zp_current_line], y
	cmp %zp_word_b
	jne .append_not_replace
	iny
	ld [zp_current_line], y
	cmp %zp_word_b + 1
	jne .append_not_replace
	; replacing the line
	iny
	ld %zp_actual_line_start
	stx

.replace_loop:
	ld input_buffer, x
	st [zp_current_line], y
	cmp #0
	jeq .run_loop
	inx
	iny
	jmp .replace_loop

.append_not_replace:
	; check if current line number is smaller than the entere numberd
	ld #3
	sty
	ld %zp_word_b + 1
	cmp [zp_current_line], y
	jcs .append_not_replace_do	; C = 1 <=> current > entered
	dey
	; high bytes are equal
	ld %zp_word_b
	cmp [zp_current_line], y
	jcc .append_search_next	; entered >= current
	; current line is after the target

.append_not_replace_do:
	ld #1
	sty

	; set new line to point to the next
	ld %zp_current_line
	st [zp_next_alloc]
	ld %zp_current_line + 1
	st [zp_next_alloc], y

	ld %zp_prev_line + 1
	cmp #0
	jeq .append_skip_prev_set
	; update previous line to point to newly allocated
	ld %zp_next_alloc
	st [zp_prev_line]
	ld %zp_next_alloc + 1
	st [zp_prev_line], y
.append_skip_prev_set:

	; set the line number
	iny		; = 2
	ld %zp_word_b
	st [zp_next_alloc], y
	iny		; = 3
	ld %zp_word_b + 1
	st [zp_next_alloc], y
	iny

	; copy the line
	ld %zp_actual_line_start
	stx
.append_copy_loop:
	ld input_buffer, x
	st [zp_next_alloc], y
	inx
	iny
	cmp #0
	jne .append_copy_loop

	; if first line = 0, set this line as first
	ld %zp_prev_line + 1	; need to check only the high byte
	cmp #0
	jne .not_first

	ld %zp_next_alloc
	st %zp_first_line
	ld %zp_next_alloc + 1
	st %zp_first_line + 1
.not_first:

	call advance_alloc
	jmp .run_loop

.append_search_next:
	ld #1
	sty
	ld %zp_current_line
	st %zp_prev_line
	ld %zp_current_line + 1
	st %zp_prev_line + 1
	ld [zp_prev_line]
	st %zp_current_line
	ld [zp_prev_line], y
	st %zp_current_line + 1
	jmp .append_search_loop

.bsp:
	dey
	jcs .bsp_at_0
	ld #8
	call send_byte
	ld #' '
	call send_byte
	ld #8
	call send_byte
	jmp .loop

.bsp_at_0:
	ld #0
	sty
	jmp .loop

.overflow:
	ld #0
	stx
.overflow_loop:
	ld overflow_message, x
	cmp #0
	jeq .run_loop
	call send_byte
	inx
	jmp .overflow_loop

advance_alloc:
	ld %zp_next_alloc
	adc #80
	st %zp_next_alloc
	ld %zp_next_alloc + 1
	adc #0
	st %zp_next_alloc + 1
	cmp #0x10
	jcs .end

	ld #no_new_lines_warning
	st %zp_line_ptr
	ld #no_new_lines_warning >> 8
	st %zp_line_ptr + 1
	call send_line
.end:
	ret

; flag C = 1 if is a digit
is_digit:
	cmp #'0'
	jmi .no
	cmp #'9' + 1
	jpl .no
	ret
.no:
	clc
	ret

; flag C = 1 if is a letter
is_letter:
	and #~' '	; upcase
	cmp #'A'
	jmi .no
	cmp #'Z' + 1
	jpl .no
	ret
.no:
	clc
	ret

.include "io.s"
.include "arith.s"
.include "basic.s"

overflow_message:
	.str "\r\n!! OVERFLOW !!\r\n\0"

no_new_lines_warning:
	.str "CAN\'T INSERT MORE LINES\r\n\0"

number_send_buf:
.equ vars, number_send_buf + 6
.equ input_buffer, vars + 52
.equ lines, input_buffer + 82
; max line length = 75 characters
; 1st 2 bytes of a line is the pointer to the next line
; 2nd 2 bytes are a line number
