.equ LEDS, 0x1000
.equ BUTTONS, 0x1001

.equ UART_DATA, 0x1002
.equ UART_STATUS, 0x1003
.equ UART_TX_BUSY, 1
.equ UART_RX_READY, 1 << 1

new_line:
	ld #'\r'
	call send_byte
	ld #'\n'
	call send_byte
	ret

read_byte:
	ld UART_STATUS
	and #UART_RX_READY
	jeq read_byte
	ld UART_DATA
	push
	ld #0
	st UART_STATUS
	pop
	ret

send_byte:
	push

.loop:
	ld UART_STATUS
	and #UART_TX_BUSY
	jne .loop

	pop
	st UART_DATA
	ret

send_number_a:
	ldx
	push

	ld #0
	st number_send_buf + 5
	ld #3
	stx

	ld #10
	st %zp_word_b
	ld #0
	st %zp_word_b + 1
	call word_div
	clc
	ld %zp_word_b
	adc #'0'
	st number_send_buf + 4

.loop:
	call word_is_0
	jcs .loop_end

	ld #10
	st %zp_word_b
	ld #0
	st %zp_word_b + 1
	call word_div
	clc
	ld %zp_word_b
	adc #'0'
	st number_send_buf, x
	dex
	jmp .loop
.loop_end:

	inx
.send_loop:
	ld number_send_buf, x
	cmp #0
	jeq .send_loop_end
	call send_byte
	inx
	jmp .send_loop
.send_loop_end:

	pop
	stx
	ret

send_line:
	ldy
	push

	ld #0
	sty
.loop:
	ld [zp_line_ptr], y
	cmp #0
	jeq .end
	call send_byte
	iny
	jmp .loop
.end:

	pop
	sty
	ret
